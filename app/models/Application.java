package models;

import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.Entity;
import java.util.Date;

/**
 * Created by DeadLox on 26/04/2014.
 */
@Entity
public class Application extends Model {
    @Required
    public String name;
    public boolean enable;
    public Date lastAwake;

    public String toString(){
        return this.name;
    }
}
