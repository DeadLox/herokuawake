package jobs;

import models.Application;
import play.Logger;
import play.Play;
import play.jobs.Job;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Cette classe permet d'être appelé par Heroku Scheduler
 * Heroku Scheduler appel la méthode Main de la classe HerokuAwake
 * Puis on récupère l'environnement Play
 * Et enfin on lance la méthode doJob de la classe
 * Created by DeadLox on 26/04/2014.
 */
public class HerokuAwake extends Job {
    private static String herokuUrl = ".herokuapp.com";

    /**
     * M2thode utilisée par HEroku Scheduler pour lancer la méthode doJob en récupérant l'environnement Play
     *
     * @param args
     */
    public static void main(String[] args) {
        File root = new File(System.getProperty("application.path"));

        if (System.getProperty("precompiled", "false").equals("true")) {
            Play.usePrecompiled = true;
        }

        try {
            Play.init(root, System.getProperty("play.id", ""));

            Class c = Play.classloader.loadClass("jobs.HerokuAwake");
            Job s = (Job) c.newInstance();
            s.run();
        } catch (Exception e) {
            Logger.error(e, e.getMessage());
        } finally {
            Play.stop();
        }
    }

    @Override
    public void doJob() throws Exception {
        List<Application> applications = Application.findAll();
        for (Application app : applications) {
            if (app.enable) {
                doAwake(app);
            }
        }
    }

    /**
     * Effectue une requête GET vers l'url de l'application pour la reveiller
     * @param app
     */
    private boolean doAwake(String app) {
        Calendar cal = Calendar.getInstance();
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        // Reveil l'application seulement après 7h du matin pour éviter les mails de notifications HEROKU
        if (hour >= 7) {
            try {
                Logger.warn("Reveil de l'application: " + app);
                URL appUrl = new URL("http://" + app + herokuUrl);
                HttpURLConnection conn = (HttpURLConnection) appUrl.openConnection();
                conn.setRequestMethod("GET");
                InputStream is = conn.getInputStream();
                return true;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    /**
     * Effectue une requête GET vers l'url de l'application pour la reveiller
     * @param app
     */
    private void doAwake(Application app) {
        if (doAwake(app.name)) {
            app.lastAwake = new Date();
            app.save();
        }
    }
}
